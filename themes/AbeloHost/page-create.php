<?php
/**
 * Template Name: Создать продукт
 */
?>
<?php get_header(); ?>

<div class="test">

    <form id="product-update" method="post" action="" class="create-product-form" enctype="multipart/form-data">
        <div><h4>Product Data</h4></div>
        <div>
            <div><label>Product Name</label></div>
            <div><input type="text" name="proname" class="proname"/></div>
        </div>
        <div>
            <div><label>Product Description</label></div>
            <div><textarea name="prodesc" class="prodesc"></textarea></div>
        </div>
        <div>
            <div><label for="date">Date: </label></div>
            <input id="date" type="date" name="date" value="" style="width: 100%"/>
        </div>
        <div>
            <label for="type">Type:</label><br>

            <select name="type">
                <option value="rare">rare</option>
                <option value="frequent">frequent</option>
                <option value="unusual">unusual</option>
            </select>
        </div>
        <div>
            <div><label>Product Price</label></div>
            <div><input type="text" name="proprice" class="proprice"/></div>
        </div>

        <div>
            <div><label>thumbnail</label></div>
            <div><input type="file" name="userfile" class="" accept="image/*" /></div>
        </div>
        <br>
        <div>
            <button type="submit" value="" class="submit">Submit</button>
        </div>
    </form>

</div>

<?php

if($_SERVER["REQUEST_METHOD"] == "POST") {

    $post_data = array(
        'post_title' => $_POST['proname'],
        'post_content' => $_POST['prodesc'],
        'post_date' => $_POST['date'],
        'post_type' => 'product',
        'post_status' => 'publish'
    );
    $post_id = wp_insert_post($post_data);

    $woo_prod = wc_get_product( $post_id );

    $woo_prod->set_regular_price($_POST['proprice'] );
    update_post_meta($post_id, 'date', $_POST['date']);
    update_post_meta($post_id, 'type', $_POST['type']);

    $woo_prod->save();

    $attachment_id = upload_user_file( $_FILES['userfile'] );
    update_post_meta($post_id, 'img', $attachment_id);
    set_post_thumbnail( $post_id, $attachment_id );

}

?>

<?php get_footer(); ?>

